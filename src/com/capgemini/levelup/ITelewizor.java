package com.capgemini.levelup;

public interface ITelewizor {

	public int getChannel();
	public void setChannel(int channelNumber);
	public void volumeUp();
	public void volumeDown();
	public String getStatus();
	public void mute();
	public void addToFavorite();
	public void printFavoriteChannels();

}
